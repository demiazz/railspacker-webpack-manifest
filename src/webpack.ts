import { compilation, Compiler } from "webpack";

export { Plugin } from "webpack";

export type Compilation = compilation.Compilation;

export type Compiler = Compiler;
