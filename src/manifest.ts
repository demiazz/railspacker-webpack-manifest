export class Manifest {
  private entrypoints: Record<string, string[]>;

  constructor() {
    this.entrypoints = {};
  }

  public addEntrypoint(name: string, assets: string[]): void {
    this.entrypoints[name] = assets;
  }

  public toJSON(): string {
    return JSON.stringify({
      entrypoints: this.entrypoints
    });
  }
}
