interface IEntrypoint {
  assets: string[];
}

interface IEntrypoints {
  [key: string]: IEntrypoint;
}

export interface IStatistics {
  entrypoints: IEntrypoints;
}
