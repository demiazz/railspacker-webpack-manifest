import { IStatistics } from "./statistics";
import { Compilation } from "./webpack";

export class Parser {
  private stats: IStatistics;

  constructor(compilation: Compilation) {
    this.stats = compilation.getStats().toJson({
      entrypoints: true
    });
  }

  public getEntrypoints() {
    return Object.entries(this.stats.entrypoints).reduce<
      Record<string, string[]>
    >((entrypoints, [name, entrypoint]) => {
      entrypoints[name] = entrypoint.assets;

      return entrypoints;
    }, {});
  }
}
