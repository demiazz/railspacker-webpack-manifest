import { Manifest } from "../manifest";

describe("Manifest", () => {
  it("empty by default", () => {
    const manifest = new Manifest();

    expect(manifest.toJSON()).toEqual(JSON.stringify({ entrypoints: {} }));
  });

  describe(".addEntrypoint", () => {
    it("adds entrypoint", () => {
      const name = "main";
      const assets = ["main.js", "main.css"];
      const manifest = new Manifest();

      manifest.addEntrypoint(name, assets);

      expect(manifest.toJSON()).toEqual(
        JSON.stringify({
          entrypoints: {
            [name]: assets
          }
        })
      );
    });
  });
});
