import webpack from "webpack";

import { Parser } from "../parser";
import { IStatistics } from "../statistics";

function createCompilationMock(
  stats: IStatistics
): webpack.compilation.Compilation {
  const mock = {
    getStats() {
      return this;
    },

    toJson() {
      return stats;
    }
  };

  return (mock as any) as webpack.compilation.Compilation;
}

describe("Parser", () => {
  describe(".getEntrypoints", () => {
    describe("when no one entrypoint is exist", () => {
      it("returns empty object", () => {
        const stats: IStatistics = {
          entrypoints: {}
        };
        const compilation: webpack.compilation.Compilation = createCompilationMock(
          stats
        );
        const parser = new Parser(compilation);

        expect(parser.getEntrypoints()).toEqual({});
      });
    });

    describe("when one entrypoint is exist", () => {
      const stats: IStatistics = {
        entrypoints: {
          main: {
            assets: ["main.js", "main.css"]
          }
        }
      };
      const compilation: webpack.compilation.Compilation = createCompilationMock(stats);
      const parser = new Parser(compilation);

      expect(parser.getEntrypoints()).toEqual({
        main: stats.entrypoints.main.assets
      });
    });

    describe("when many entrypoints are exist", () => {
      const stats: IStatistics = {
        entrypoints: {
          main: {
            assets: ["main.js", "main.css"]
          },
          vendor: {
            assets: ["vendor.js", "vendor.css"]
          }
        }
      };
      const compilation: webpack.compilation.Compilation = createCompilationMock(stats);
      const parser = new Parser(compilation);

      expect(parser.getEntrypoints()).toEqual({
        main: stats.entrypoints.main.assets,
        vendor: stats.entrypoints.vendor.assets
      });
    });
  });
});
