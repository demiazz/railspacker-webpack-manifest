<h1 align="center">@railspacker / webpack-manifest</h1>

<div align="center">
  :package:&nbsp;&nbsp;&nbsp;&nbsp;:pen:&nbsp;&nbsp;&nbsp;&nbsp;:lock:
</div>
<div align="center">
  <strong>Webpack plugin for generating a manifest which compatible with RailsPacker gem</strong>
  <br />
  Pack, take stock and document...
</div>

<br />

<div align="center">
  <sub>Built with ❤︎ by
  <a href="https://twitter.com/demiazz">demiazz</a>.
  Sponsored by <a href="http://evilmartians.com">Evil Martians</a>.
</div>
